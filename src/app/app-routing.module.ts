import {NgModule} from '@angular/core'
import {RouterModule, Routes} from '@angular/router'
import {LayoutComponent} from './layout/layout.component'
/**
 * 路由
 */
const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'index'},
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'index',
        loadChildren: () => import('./page/index/index.module').then((m) => m.IndexModule),
      },
    ],
  },
  {
    path: 'sign-up',
    loadChildren: () => import('./page/sign-up/sign-up.module').then((m) => m.SignUpModule),
  },
  {
    path: 'sign-in',
    loadChildren: () => import('./page/sign-in/sign-in.module').then((m) => m.SignInModule),
  },
]
/**
 * 根路由模組
 */
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
