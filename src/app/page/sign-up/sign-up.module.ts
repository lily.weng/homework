import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'

import {SignUpRoutingModule} from './sign-up-routing.module'
import {SignUpComponent} from './sign-up.component'

import {MatInputModule} from '@angular/material/input'
import {MatIconModule} from '@angular/material/icon'

/**
 * 登入頁模組
 */
@NgModule({
  declarations: [SignUpComponent],
  imports: [CommonModule, SignUpRoutingModule, MatInputModule, MatIconModule],
})
export class SignUpModule {}
