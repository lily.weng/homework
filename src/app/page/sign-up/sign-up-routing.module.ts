import {NgModule} from '@angular/core'
import {RouterModule, Routes} from '@angular/router'
import {SignUpComponent} from './sign-up.component'
/**
 * 登入頁路由
 */
const routes: Routes = [{path: '', component: SignUpComponent}]
/**
 * 登入頁路由模組
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignUpRoutingModule {}
